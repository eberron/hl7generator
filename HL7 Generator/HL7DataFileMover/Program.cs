﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HL7DataFileMover
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() == 2)
            {
                String inputFilePath = args[0],
                       outputPath = args[1];
                
                //--Move the admit/discharge files
                if (Directory.Exists(inputFilePath) && Directory.Exists(outputPath))
                {
                    IEnumerable<string> filesToMove = Directory.EnumerateFiles(inputFilePath);
                    Dictionary<long, List<string>> timePathMap = new Dictionary<long, List<string>>();
                    long nowTicks = DateTime.UtcNow.Ticks;

                    foreach (string fileToMove in filesToMove)
                    {
                        try
                        {
                            string[] timeKey = Path.GetFileNameWithoutExtension(fileToMove).Split(new string[] { "_" }, StringSplitOptions.None);
                            if (null != timeKey && timeKey.Count() > 0)
                            {
                                if (Convert.ToInt64(timeKey[0]) <= nowTicks)
                                {
                                    Directory.Move(fileToMove, Path.Combine(outputPath, Path.GetFileName(fileToMove)));
                                }
                            }
                        }
                        catch (Exception ex)
                        { 
                        }
                    }
                }
            }
        }
    }
}
