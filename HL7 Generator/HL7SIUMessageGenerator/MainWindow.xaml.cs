﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HL7SIUMessageGenerator.DataModel;
using HL7SIUMessageGenerator.Utilities;
using System.Diagnostics;
//using ExtWpf = Microsoft.Windows.Controls;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using System.IO;

namespace HL7SIUMessageGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int numberOfAppointments = 0;
        private int appointmentDuration = 0;
        private DateTime selectedDate;
        private int selectedHour;
        private int selectedMinute;
        private IList<Dictator> dictatorList = null;
        private IList<Patient> patientList = null;
        private IList<Reason> reasonList = null;
        private IList<CareTeamMember> careTeamList = null;
        private IList<Location> locationList = null;

        private string templateString = string.Empty;
        private static Random random = new Random();
        private static Random randomPatient = new Random();
        private static string logFileNameSuffix = "log.txt";

        private static string defaultDictatorFile = "Dictators.txt";
        private static string defaultCareTeamFile = "CareTeam.txt";
        private static string defaultReasonsFile = "Reasons.txt";
        private static string defaultPatientsFile = "Patients.txt";
        private static string defaultLocationsFile = "Locations.txt";
        private static string defaultOutputDirectory = "Generated HL7";
        private static string defaultOutputDischargeDirectory = "Discharge";
        private static string defaultTemplateFile = "";

        public MainWindow()
        {
            InitializeComponent();
            datePicker1.SelectedDate = DateTime.Now.AddDays(1);
            timePicker1.Value = new DateTime(datePicker1.SelectedDate.Value.Year, datePicker1.SelectedDate.Value.Month, datePicker1.SelectedDate.Value.Day, 8, 0, 0);

            string defaultDirectory = Environment.CurrentDirectory;
            txtDictators.Text = System.IO.Path.Combine(defaultDirectory, defaultDictatorFile);
            txtLocations.Text = System.IO.Path.Combine(defaultDirectory, defaultLocationsFile);
            txtReason.Text = System.IO.Path.Combine(defaultDirectory, defaultReasonsFile);
            txtPatients.Text = System.IO.Path.Combine(defaultDirectory, defaultPatientsFile);
            txtCareTeam.Text = System.IO.Path.Combine(defaultDirectory, defaultCareTeamFile);

            txtOutputFolder.Text = System.IO.Path.Combine(defaultDirectory, defaultOutputDirectory);
            txtTemplateFile.Text = System.IO.Path.Combine(defaultDirectory, defaultTemplateFile);
            txtInpatientMrnStartNumber.Text = Properties.Settings.Default.LastInPatientMRNStartNumber.ToString();
            txtInpatientVisitStartNumber.Text = Properties.Settings.Default.LastInPatientVisitStartNumber.ToString();

            VisitGenerator.Instance.LoadUsedNames(System.IO.Path.Combine(defaultDirectory, "usedNames.txt"));
        }

        private String showFileDialog(String fileName, String extension = ".txt", String filter = "Text documents (.txt)|*.txt")
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = System.IO.Path.GetDirectoryName(fileName);
            dlg.FileName = System.IO.Path.GetFileName(fileName);
            dlg.DefaultExt = extension;
            dlg.Filter = filter;
            
            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            String selectedFile = "";
            if (result == true)
            {
                // Open document
                selectedFile = dlg.FileName;
            }

            return selectedFile;
        }

        private void btnBrowseTemplateFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.SelectedPath = txtTemplateFile.Text;
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                // Open document
                txtTemplateFile.Text = dlg.SelectedPath;
            }
        }

        private void btnBrowseDictators_Click(object sender, RoutedEventArgs e)
        {
            String path = showFileDialog(txtDictators.Text);
            if (!string.IsNullOrEmpty(path))
                txtDictators.Text = path;
        }

        private void btnBrowsePatients_Click(object sender, RoutedEventArgs e)
        {
            String path = showFileDialog(txtPatients.Text);
            if (!string.IsNullOrEmpty(path))
                txtPatients.Text = path;
        }

        private void txtBrowseReferring_Click(object sender, RoutedEventArgs e)
        {
            String path = showFileDialog(txtCareTeam.Text);
            if (!string.IsNullOrEmpty(path))
                txtCareTeam.Text = path;
        }

        private void btnBrowseReason_Click(object sender, RoutedEventArgs e)
        {
            String path = showFileDialog(txtReason.Text);
            if (!string.IsNullOrEmpty(path))
                txtReason.Text = path;
        }

        private void btnLocations_Click(object sender, RoutedEventArgs e)
        {
            String path = showFileDialog(txtLocations.Text);
            if (!string.IsNullOrEmpty(path))
                txtLocations.Text = path;
        }

        private void btnOutputFolder_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            // Show open file dialog box
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                // Open document
                txtOutputFolder.Text = dlg.SelectedPath;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            String error = validateUI();
            if (!string.IsNullOrEmpty(error))
            {
                MessageBox.Show(error);
                return;
            }
            
            int maxLocationCount = 0;
            foreach (Dictator dic in dictatorList)
            {
                if (dic.LocationList.Count > maxLocationCount)
                {
                    maxLocationCount = dic.LocationList.Count;
                }
            }

            if (patientList.Count < (numberOfAppointments * maxLocationCount))
            {
                MessageBox.Show("Number of patients are less than number of appointments. Please add " + ((numberOfAppointments * maxLocationCount) - patientList.Count).ToString() + " more patients.");
                return;
            }

            Properties.Settings.Default.LastInPatientMRNStartNumber = Convert.ToInt32(txtInpatientMrnStartNumber.Text);
            Properties.Settings.Default.LastInPatientVisitStartNumber = Convert.ToInt32(txtInpatientVisitStartNumber.Text);
            Properties.Settings.Default.Save();

            selectedDate = datePicker1.SelectedDate.Value;
            selectedHour = timePicker1.Value.Value.Hour;
            selectedMinute = timePicker1.Value.Value.Minute;
            
            //the basic configuration expects the progress bar to be updated
            //by the worker thread
            ProgressDialog dlg = new ProgressDialog();
            dlg.Owner = this;
            dlg.DialogText = "Generating HL7 SIU12 Messages";
            dlg.AutoIncrementInterval = 500;
            dlg.Closing += new CancelEventHandler(progressClosingHandler);
            
            //we cannot access the user interface on the worker thread, but we
            //can submit an arbitrary object to the RunWorkerThread method
            GeneratorData data = new GeneratorData();

            String templateFilePath = txtTemplateFile.Text;
            if(data.InPatient = cbMode.IsChecked.Value)
            {
                templateFilePath = System.IO.Path.Combine( txtTemplateFile.Text, "InPatientTemplate.hl7");
            }
            else 
            {
                templateFilePath = System.IO.Path.Combine(txtTemplateFile.Text, "OutPatientTemplate.hl7");
            }
            templateString = FileTools.ReadFileString(templateFilePath);            

            data.Template = templateString;
            data.DictatorList = dictatorList;
            data.PatientList = patientList;
            data.ReasonList = reasonList;
            data.CareTeamList = careTeamList;
            data.OutputFolder = txtOutputFolder.Text;
            data.OutputDischargeFolder = System.IO.Path.Combine(txtOutputFolder.Text, defaultOutputDischargeDirectory);
            data.AppointmentDate = selectedDate;
            data.AppointmentHour = selectedHour;
            data.AppointmentMinute = selectedMinute;
            data.AppointmentDuration = appointmentDuration;
            
            
            data.LocationList = locationList;
            data.careTeamRoles = new List<CareTeamRole>();
            data.careTeamRoles.Add( CareTeamRole.Admitting);
            data.careTeamRoles.Add( CareTeamRole.Referring);
            
            if(cbAttending.IsChecked.Value)
                data.careTeamRoles.Add(CareTeamRole.Attending);

            if(cbConsulting.IsChecked.Value)
                data.careTeamRoles.Add(CareTeamRole.Consulting);


            //data.DischargeFactor = (float)Convert.ToDouble(txtDischargeIntervalFactor.Text);
            //data.DischargeFrequency = Convert.ToInt32(txtDischargeFrequency.Text);
            //data.DischargeInterval = Convert.ToInt32(txtDischargeInterval.Text);

            data.ImportDayOffset = Convert.ToInt32(txtTimeStampOffset.Text);
            data.UseGeneratedPatient = (bool)cbUseRandomGeneratedPatient.IsChecked;
            data.UseDischargeFolder = (bool)cbUseDischargeFolder.IsChecked;
            data.DaysToGenerate = Convert.ToInt32(txtDaysToGenerate.Text);
            data.ItemsPerDay = Convert.ToInt32(txtItemsPerDay.Text);
            data.NumberOfAppointments = numberOfAppointments;
            data.DischargeList = txtDischargeList.Text;

            

            //start processing and submit the start value
            if(data.InPatient)
                dlg.RunWorkerThread(data, createInpatientHL7Messages);
            else
                dlg.RunWorkerThread(data, createOutpatientHL7Messages);

            MessageBox.Show("HL7 SIU12 Message creation complete.");

            txtInpatientMrnStartNumber.Text = Properties.Settings.Default.LastInPatientMRNStartNumber.ToString();
            txtInpatientVisitStartNumber.Text = Properties.Settings.Default.LastInPatientVisitStartNumber.ToString();
        }

        void progressClosingHandler(object sender, CancelEventArgs e)
        {
            
        }

        private long[] generateDischargeKeys(long frequency, long interval, float factor, String outputPath)
        {
            double dischargeIntervalMS = interval * 3600000;
            long[] dischargeTimes = new long[frequency];
            for (int index = 0; index < frequency; index++)
            {
                dischargeTimes[index] = (long)dischargeIntervalMS;
                dischargeIntervalMS *= factor;
            }
            return dischargeTimes;
        }

        private void createOutpatientHL7Messages(object sender, DoWorkEventArgs e)
        {
            //the sender property is a reference to the dialog's BackgroundWorker
            //component
            BackgroundWorker worker = (BackgroundWorker)sender;
            GeneratorData data = (GeneratorData)e.Argument;

            DateTime apptDateTime;
            MasterData masterData = null;
            
            string  folder = data.OutputFolder,
                    fileName = string.Empty,
                    logFileName = string.Empty;

            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            String logFileFolder = folder + "\\Logs";
            if (!System.IO.Directory.Exists(logFileFolder))
            {
                System.IO.Directory.CreateDirectory(logFileFolder);
            }

            data.NumberOfAppointments = data.DaysToGenerate * data.ItemsPerDay;

            DateTime now = DateTime.Now;
            String dateTimeStamp = String.Format("{0:d4}{1:d2}{2:d2}-{3:d2}{4:d2}{5:d2}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            FileLogger fileLogger = new FileLogger(logFileFolder, String.Format("{0}_log.txt", dateTimeStamp));
            
            string msg = "Generating HL7 SIU12 Message for {0}...";
            int count = 0;
            IList<Patient> tempPatientList = null;
            IList<CareTeamMember> tempCareTeamList = null;

            List<JsonPatient> jsonPatients = new List<JsonPatient>();

            try
            {
                DateTime importDateTime;
                String importDateTimeMS;
                
                foreach (Dictator dictator in data.DictatorList)
                {
                    int progress = (int)((count / data.DictatorList.Count) * 100);
                    msg = String.Format(msg, dictator.LastName + " " + dictator.FirstName);
                    worker.ReportProgress(progress, msg);
                    count++;

                    apptDateTime = new DateTime(data.AppointmentDate.Year, data.AppointmentDate.Month, data.AppointmentDate.Day, data.AppointmentHour, data.AppointmentMinute, 0);

                    int randomPatientPosition = 0;
                    for (int days = 0; days < data.DaysToGenerate; days++)
                    {
                        // Keep a temp copy of patientList
                        tempPatientList = new List<Patient>(data.PatientList);

                        foreach (Location location in dictator.LocationList)
                        {
                            Location[] relevantLocations = data.LocationList.Where(rl => rl.Id == location.Id).ToArray();
                            for (int index = 0; index < data.ItemsPerDay; index++)
                            {
                                //--Set the import stamp(so the appointment HL7 occurs prior to the appointment 
                                //  e.g. it shows up in the app ahead of the appointment)
                                //  This is used by the file mover app
                                if (0 == data.ImportDayOffset)
                                {
                                    importDateTime = new DateTime(apptDateTime.Year, apptDateTime.Month, apptDateTime.Day, 0, 0, 0);
                                }
                                else
                                {
                                    importDateTime = new DateTime(apptDateTime.Year, apptDateTime.Month, apptDateTime.Day, 0, 0, 0).Subtract(new TimeSpan(data.ImportDayOffset, 0, 0, 0));
                                }
                                importDateTimeMS = importDateTime.ToUniversalTime().Ticks.ToString();

                                tempCareTeamList = new List<CareTeamMember>(data.CareTeamList);

                                fileName = apptDateTime.ToString("yyyyMMdd") + "_" + location.Id + "_" + dictator.PhysicianCode + (index + 1).ToString() + ".hl7";
                                logFileName = apptDateTime.ToString("yyyyMMdd") + "_" + location.Id + "_" + dictator.LastName.Trim() + logFileNameSuffix;

                                masterData = new MasterData();
                                masterData.Referring = new CareTeamMember();
                                masterData.Attending = new CareTeamMember();
                                masterData.Consulting = new CareTeamMember();

                                masterData.Template = data.Template;
                                masterData.Dictator = dictator;
                                masterData.Location = location;
                                if (null != relevantLocations && relevantLocations.Count() > 0)
                                {
                                    masterData.Location = relevantLocations[random.Next(relevantLocations.Count())];
                                }

                                int randomCareTeamIndex = 0;
                                foreach (CareTeamRole role in data.careTeamRoles)
                                {
                                    randomCareTeamIndex = random.Next(tempCareTeamList.Count);
                                    switch (role)
                                    {
                                        case CareTeamRole.Attending:
                                            masterData.Attending = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                        case CareTeamRole.Consulting:
                                            masterData.Consulting = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                        case CareTeamRole.Referring:
                                            masterData.Referring = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                    }
                                    tempCareTeamList.RemoveAt(randomCareTeamIndex);
                                }

                                masterData.Reason = data.ReasonList[random.Next(reasonList.Count)];
                                masterData.AppointmentDate = apptDateTime;

                                // Get random patient position
                                randomPatientPosition = randomPatient.Next(tempPatientList.Count);

                                // Use the patient
                                masterData.Patient = tempPatientList[randomPatientPosition];
                                masterData.Patient.Visit = masterData.Dictator.PhysicianCode.Trim() + masterData.Patient.Mrn.ToString() + masterData.AppointmentDate.Month.ToString().PadLeft(2, '0') + masterData.AppointmentDate.Day.ToString().PadLeft(2, '0'); ;

                                // Remove the patient from the list so that next time same patient is not used
                                tempPatientList.RemoveAt(randomPatientPosition);

                                StringBuilder hl7Admission = null;
                                hl7Admission = FileTools.GetSingleOutPatientHL7Message(masterData);

                                System.IO.File.WriteAllText(folder + "\\" + importDateTimeMS + "_" + fileName, hl7Admission.ToString());

                                fileLogger.LogInfo(masterData.Patient.LastName, masterData.Patient.FirstName, masterData.Patient.Mrn, 
                                                   masterData.Patient.Visit, masterData.AppointmentDate.ToString("yyyyMMddHHmmss"), masterData.Reason.ReasonText, 
                                                   masterData.Location.Description, masterData.AppointmentDate.ToString(), masterData.DischargeDate.ToString(), 
                                                   masterData.DischargeDate.ToUniversalTime().Ticks.ToString(), dictator.LastName, dictator.FirstName, dictator.PhysicianCode,
                                                   "", masterData.Patient.DateOfBirth, masterData.Patient.Gender, FileTools.ExpandPhoneNumber("888" + masterData.Patient.Mrn.Trim()), masterData.Attending.LastName, masterData.Attending.FirstName, 
                                                   masterData.Consulting.LastName, masterData.Consulting.FirstName, masterData.Referring.LastName, masterData.Referring.FirstName);

                                jsonPatients.Add(new JsonPatient(masterData, dictator));

                                apptDateTime = apptDateTime.AddMinutes(data.AppointmentDuration);
                            }
                            apptDateTime = apptDateTime.AddMinutes(-data.AppointmentDuration);
                            apptDateTime = apptDateTime.AddHours(1);
                        }

                        DateTime tempDateTime = apptDateTime.Add(new TimeSpan(1, 0, 0, 0));
                        apptDateTime = new DateTime(tempDateTime.Year, tempDateTime.Month, tempDateTime.Day, data.AppointmentHour, data.AppointmentMinute, 0);
                    }
                }

                string jsonOutputPath = System.IO.Path.Combine(logFileFolder, String.Format("patients_{0}.json", dateTimeStamp));
                generateJsonPatientOutput(jsonOutputPath, jsonPatients);

            }
            catch (Exception ex)
            {
                String error = ex.Message;

            }
        }

        private String appendImportTimeStamp(DateTime startDate, String fileName, long dayOffset)
        {
            return "";
        }

        private void createInpatientHL7Messages(object sender, DoWorkEventArgs e)
        {
            //the sender property is a reference to the dialog's BackgroundWorker
            //component
            BackgroundWorker worker = (BackgroundWorker)sender;
            GeneratorData data = (GeneratorData)e.Argument;

            DateTime apptDateTime;
            MasterData masterData = null;

            string  folder = data.OutputFolder,
                    dischargeFolder = data.OutputDischargeFolder,
                    fileName = string.Empty,
                    logFileName = string.Empty;

            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            if (!System.IO.Directory.Exists(dischargeFolder))
            {
                System.IO.Directory.CreateDirectory(dischargeFolder);
            }

            String logFileFolder = folder + "\\Logs";
            if (!System.IO.Directory.Exists(logFileFolder))
            {
                System.IO.Directory.CreateDirectory(logFileFolder);
            }
            data.NumberOfAppointments = data.DaysToGenerate * data.ItemsPerDay;

            //--Generate discharge times
            //long[] dischargeTimes = generateDischargeKeys(data.DischargeFrequency, data.DischargeInterval, data.DischargeFactor, data.OutputFolder);
            long[] dischargeTimes = null;
            if (data.DischargeList.Length > 0)
            {
                List<long> discharges = new List<long>();
                foreach (String dischargeTime in data.DischargeList.Split(new char[] { ',' }))
                {
                    discharges.Add(Convert.ToInt32(dischargeTime) * 3600000);
                }
                dischargeTimes = discharges.ToArray();
            }
            else 
            {
                dischargeTimes = new long[] { 24 * 3600000 };
            }

            DateTime now = DateTime.Now;
            String dateTimeStamp = String.Format("{0:d4}{1:d2}{2:d2}-{3:d2}{4:d2}{5:d2}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            FileLogger fileLogger = new FileLogger(logFileFolder, String.Format("{0}_log.txt", dateTimeStamp));

            string msg = "Generating HL7 SIU12 Message for {0}...";
            int count = 0;
            IList<CareTeamMember> tempCareTeamList = null;
            IList<Patient> tempPatientList = null;

            List<JsonPatient> jsonPatients = new List<JsonPatient>();
            try
            {
                String importDateTimeMS = "";
                DateTime importDateTime;

                foreach (Dictator dictator in data.DictatorList)
                {
                    int progress = (int)((count / data.DictatorList.Count) * 100);
                    msg = String.Format(msg, dictator.LastName + " " + dictator.FirstName);
                    worker.ReportProgress(progress, msg);
                    count++;

                    //--Reset the appt time to the start date (in the UI)
                    apptDateTime = new DateTime(data.AppointmentDate.Year, data.AppointmentDate.Month, data.AppointmentDate.Day, data.AppointmentHour, data.AppointmentMinute, 0);

                    int randomPatientPosition = 0;
                    int itemsGenerated = 0;
                    for (int days = 0; days < data.DaysToGenerate; days++)
                    {
                        // Keep a temp copy of patientList
                        tempPatientList = new List<Patient>(data.PatientList);

                        foreach (Location location in dictator.LocationList)
                        {
                            //fileLogger.WriteUserInfo(formatUserInfoLogMessage(dictator, location));

                            Location[] relevantLocations = data.LocationList.Where(rl => rl.Id == location.Id).ToArray();
                            for (int index = 0; index < data.ItemsPerDay; index++)
                            {
                                //--Set the import stamp(so the appointment HL7 occurs prior to the appointment 
                                //  e.g. it shows up in the app ahead of the appointment)
                                //  This is used by the file mover app
                                if (0 == data.ImportDayOffset)
                                {
                                    importDateTime = new DateTime(apptDateTime.Year, apptDateTime.Month, apptDateTime.Day, 0, 0, 0);
                                }
                                else
                                {
                                    importDateTime = new DateTime(apptDateTime.Year, apptDateTime.Month, apptDateTime.Day, 0, 0, 0).Subtract(new TimeSpan(data.ImportDayOffset, 0, 0, 0));
                                }
                                importDateTimeMS = importDateTime.ToUniversalTime().Ticks.ToString();

                                tempCareTeamList = new List<CareTeamMember>(data.CareTeamList);

                                fileName = apptDateTime.ToString("yyyyMMdd") + "_" + location.Id + "_" + dictator.PhysicianCode + (index + 1).ToString() + ".hl7";
                                logFileName = apptDateTime.ToString("yyyyMMdd") + "_" + location.Id + "_" + dictator.LastName.Trim() + logFileNameSuffix;

                                masterData = new MasterData();
                                masterData.Referring = new CareTeamMember();
                                masterData.Attending = new CareTeamMember();
                                masterData.Consulting = new CareTeamMember();

                                masterData.Template = data.Template;
                                masterData.Dictator = dictator;
                                masterData.Location = location;
                                if (null != relevantLocations && relevantLocations.Count() > 0)
                                {
                                    masterData.Location = relevantLocations[random.Next(relevantLocations.Count())];
                                }

                                int randomCareTeamIndex = 0;
                                foreach (CareTeamRole role in data.careTeamRoles)
                                {
                                    randomCareTeamIndex = random.Next(tempCareTeamList.Count);
                                    switch (role)
                                    {
                                        case CareTeamRole.Attending:
                                            masterData.Attending = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                        case CareTeamRole.Consulting:
                                            masterData.Consulting = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                        case CareTeamRole.Referring:
                                            masterData.Referring = tempCareTeamList[randomCareTeamIndex];
                                            break;
                                    }
                                    tempCareTeamList.RemoveAt(randomCareTeamIndex);
                                }

                                masterData.Reason = data.ReasonList[random.Next(reasonList.Count)];
                                masterData.AppointmentDate = apptDateTime;


                                // Get random patient position
                                if (data.UseGeneratedPatient)
                                {
                                    Patient randomPatient = VisitGenerator.Instance.GenerateRandomPatient();
                                    masterData.Patient = randomPatient;
                                }
                                else
                                {
                                    randomPatientPosition = randomPatient.Next(tempPatientList.Count);

                                    // Use the patient
                                    masterData.Patient = tempPatientList[randomPatientPosition];
                                    masterData.Patient.Visit = masterData.Dictator.PhysicianCode.Trim() + masterData.Patient.Mrn.ToString() + masterData.AppointmentDate.Month.ToString().PadLeft(2, '0') + masterData.AppointmentDate.Day.ToString().PadLeft(2, '0'); ;

                                    // Remove the patient from the list so that next time same patient is not used
                                    tempPatientList.RemoveAt(randomPatientPosition);
                                }

                                StringBuilder hl7Admission = null;
                                StringBuilder hl7Discharge = null;

                                DateTime dischargeDate = masterData.AppointmentDate.AddMilliseconds(dischargeTimes[random.Next(dischargeTimes.Count())]);
                                Trace.WriteLine(String.Format("Discharge Date is now {0} - {1} - {2}", apptDateTime.ToString(), dischargeDate.ToString(), dischargeDate.Ticks));

                                masterData.DischargeDate = dischargeDate;
                                hl7Admission = FileTools.GetSingleInPatientHL7Message(masterData);
                                hl7Discharge = FileTools.GetSingleInPatientDischargeHL7Message(masterData);

                                if (data.UseDischargeFolder)
                                    System.IO.File.WriteAllText(dischargeFolder + "\\" + masterData.DischargeDate.ToUniversalTime().Ticks.ToString() + "_D_" + fileName, hl7Discharge.ToString());
                                else
                                    System.IO.File.WriteAllText(folder + "\\" + masterData.DischargeDate.ToUniversalTime().Ticks.ToString() + "_D_" + fileName, hl7Discharge.ToString());

                                System.IO.File.WriteAllText(folder + "\\" + importDateTimeMS + "_" + fileName, hl7Admission.ToString());

                                //TODO:DOB should be m//d//yy
                                fileLogger.LogInfo(masterData.Patient.LastName, masterData.Patient.FirstName, masterData.Patient.Mrn, masterData.Patient.Visit, 
                                                   masterData.AppointmentDate.ToString("yyyyMMddHHmmss"), masterData.Reason.ReasonText, masterData.Location.Description, 
                                                   masterData.AppointmentDate.ToString(), masterData.DischargeDate.ToString(), masterData.DischargeDate.ToUniversalTime().Ticks.ToString(),
                                                   dictator.LastName, dictator.FirstName, dictator.PhysicianCode, "", masterData.Patient.DateOfBirth, masterData.Patient.Gender, FileTools.ExpandPhoneNumber("888" + masterData.Patient.Mrn.Trim()), 
                                                   masterData.Attending.LastName, masterData.Attending.FirstName, masterData.Consulting.LastName, masterData.Consulting.FirstName, masterData.Referring.LastName, masterData.Referring.FirstName);

                                jsonPatients.Add(new JsonPatient(masterData, dictator));

                                apptDateTime = apptDateTime.AddMinutes(data.AppointmentDuration);
                                itemsGenerated++;
                            }
                            apptDateTime = apptDateTime.AddMinutes(-data.AppointmentDuration);
                            apptDateTime = apptDateTime.AddHours(1);
                        }
                 
                        DateTime tempDateTime = apptDateTime.Add(new TimeSpan(1, 0, 0, 0));
                        apptDateTime = new DateTime(tempDateTime.Year, tempDateTime.Month, tempDateTime.Day, data.AppointmentHour, data.AppointmentMinute, 0);
                        //fileLogger.WriteUserInfo("New Day");
                    }
                }

                string jsonOutputPath = System.IO.Path.Combine(logFileFolder, String.Format("patients_{0}.json", dateTimeStamp));
                generateJsonPatientOutput(jsonOutputPath, jsonPatients);

            }
            catch (Exception ex)
            {
                String error = ex.Message;

            }
        }

        private void generateJsonPatientOutput(string jsonOutputPath, List<JsonPatient> jsonPatients)
        {
            JsonPatient[] jsonPatientArray = jsonPatients.ToArray();
            using (FileStream inputStream = new FileStream(jsonOutputPath, FileMode.Create))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(JsonPatient[]), new List<Type>() { typeof(JsonPatient)});
                serializer.WriteObject(inputStream, jsonPatientArray);
            }
        }

        private String formatUserInfoLogMessage(Dictator dictator, Location location)
        {
            return String.Format("{0},{1}({2}) - {3}", dictator.LastName, dictator.FirstName, dictator.PhysicianCode, location.Description);
        }

        private String validateUI()
        {
            String error = "";
            //--This should be using WPF's validation mechanisms... :(
            //  Overall, this is horrible
            dictatorList = Dictator.GetDictators(txtDictators.Text);
            patientList = Patient.GetPatients(txtPatients.Text);
            reasonList = Reason.GetReasons(txtReason.Text);
            careTeamList = CareTeamMember.GetCareTeamMembers(txtCareTeam.Text);
            locationList = Location.GetLocations(txtLocations.Text);

            if (string.IsNullOrEmpty(txtTemplateFile.Text))
            {
                error = "Please select a valid Template file.";
            }
            else if (string.IsNullOrEmpty(txtDictators.Text))
            {
                error = "Please select a valid Dictator file.";
            }
            else if (string.IsNullOrEmpty(txtPatients.Text))
            {
                error = "Please select a valid Patients file.";
            }
            else if (string.IsNullOrEmpty(txtCareTeam.Text))
            {
                error = "Please select a valid Referrings file.";
            }
            else if (string.IsNullOrEmpty(txtReason.Text))
            {
                error = "Please select a valid Reasons file.";
            }
            else if (string.IsNullOrEmpty(txtDuration.Text))
            {
                error = "Please enter duration of visits.";
            }
            else if (!datePicker1.SelectedDate.HasValue)
            {
                error = "Please select a valid date for appointments.";
            }
            else if (null == dictatorList || dictatorList.Count == 0)
            {
                error = "No Dictators found in File";
            }
            else if (null == patientList || patientList.Count == 0)
            {
                error = "No Patients found in File";
            }

            else if (null == reasonList || reasonList.Count == 0)
            {
                error = "No Reasons found in File";
            }
            else if (null == careTeamList || careTeamList.Count == 0)
            {
                error = "No Referring Physicians found in File";
            }

            if (error == "")
            {
                if (!string.IsNullOrEmpty(txtDuration.Text))
                {
                    try
                    {
                        appointmentDuration = Int32.Parse(txtDuration.Text);
                    }
                    catch
                    {
                        error = "Please enter a valid duration of appointments.";
                    }
                }
            }
            
            return error;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            VisitGenerator.Instance.SaveUsedNames();
        }
    }
}
