﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
 
namespace HL7SIUMessageGenerator.Utilities
{
    public class FileLogger
    {
        private string Folder { get; set; }
        private string FileName { get; set; }
        private string FilePath
        {
            get
            {
                return Folder + "\\" + FileName;
            }
        }
        /// <summary>
        /// public constructor. ALways use constructor
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileName"></param>
        public FileLogger(string folder, string fileName)
        {
            this.Folder = folder;
            this.FileName = fileName;
            CreateNewFileLogger();
        }
        /// <summary>
        /// Write log information to log file
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <param name="mrn"></param>
        /// <param name="visitId"></param>
        /// <param name="apptTime"></param>
        /// <param name="apptReason"></param>
        /// <param name="locationText"></param>
        public void LogInfo(string lastName, string firstName, string mrn, string visitId, string apptTime, string apptReason, string locationText, string admitDate, string dischargeDate, string dischargeCode, string dictatorLast, string dictatorFirst, string dictatorId, string age, string dateOfBirth, string gender, string phoneNumber, string attendingLastName, string attendingFirstName, string consultingLastName, string consultingFirstName, string referringLastName, string referringFirstName)
        {
            StringBuilder logString = new StringBuilder();
            logString.Append(lastName);
            logString.Append("|");

            logString.Append(firstName);
            logString.Append("|");

            logString.Append(lastName + ", " + firstName);
            logString.Append("|");

            logString.Append(mrn);
            logString.Append("|");

            logString.Append(visitId);
            logString.Append("|");

            logString.Append(apptTime);
            logString.Append("|");

            logString.Append(apptReason);
            logString.Append("|");

            logString.Append(locationText);
            logString.Append("|");

            logString.Append(admitDate);
            logString.Append("|");

            logString.Append(dischargeDate);
            logString.Append("|");

            logString.Append(dischargeCode);
            logString.Append("|");

            logString.Append(dictatorLast);
            logString.Append("|");

            logString.Append(dictatorFirst);
            logString.Append("|");

            logString.Append(dictatorId);
            logString.Append("|");

            logString.Append(dateOfBirth);
            logString.Append("|");

            logString.Append(age);
            logString.Append("|");

            logString.Append(gender);
            logString.Append("|");

            logString.Append(phoneNumber);
            logString.Append("|");

            logString.Append(attendingLastName);
            logString.Append("|");

            logString.Append(attendingFirstName);
            logString.Append("|");

            logString.Append(attendingFirstName + " " + attendingLastName);
            logString.Append("|");

            logString.Append(consultingLastName);
            logString.Append("|");

            logString.Append(consultingFirstName);
            logString.Append("|");

            logString.Append(consultingFirstName + " " + consultingLastName);
            logString.Append("|");

            logString.Append(referringLastName);
            logString.Append("|");

            logString.Append(referringFirstName);
            logString.Append("|");

            logString.Append(referringFirstName + " " + referringLastName);
            logString.Append("|");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(FilePath, true))
            {
                file.WriteLine(logString.ToString());
            }
        }

        public void WriteUserInfo(String userInfo)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(FilePath, true))
            {
                file.WriteLine();
                
                file.WriteLine(String.Format("{0}", userInfo));
                file.WriteLine("---------------------------------------------");
            }
        }

        /// <summary>
        /// Create header for log file
        /// </summary>
        private void WriteHeader()
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(FilePath, true))
            {
                file.WriteLine("LastName|FirstName|MRN|VisitID|ApptTime|ApptReason|LocationText|AdmitDate|DischargeDate|DischargeCode|DictatorLast|DictatorFirst|DictatorId");
            }
        }
        /// <summary>
        /// Create a new log file with header
        /// </summary>
        private void CreateNewFileLogger()
        {
            if (!Directory.Exists(this.Folder))
            {
                Directory.CreateDirectory(this.Folder);
            }
            if (!File.Exists(FilePath))
            {
                FileStream fs = File.Create(FilePath);
                fs.Close();
                WriteHeader();
            }
        }
    }
}
