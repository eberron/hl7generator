﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;

namespace HL7SIUMessageGenerator.Utilities
{
    /// <summary>
    /// CSV parser class
    /// </summary>
    public class CSVParser
    {
        /// <summary>
        /// Method to parse a CSV file and return a data table
        /// </summary>
        /// <param name="path">File path</param>
        /// <returns>DataTable</returns>
        public static DataTable ParseCSV(string path)
        {
            if (!File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            //create the "database" connection string
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;"
              + "Data Source=\"" + dir + "\\\";"
              + "Extended Properties=\"text;HDR=No;FMT=Delimited\"";

            //create the database query
            string query = "SELECT * FROM [" + file + "]";

            //create a DataTable to hold the query results
            DataTable dTable = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, connString);

            try
            {
                //fill the DataTable
                dAdapter.Fill(dTable);
            }
            catch (InvalidOperationException ex)
            {
                throw ex;  
            }

            dAdapter.Dispose();

            return dTable;
        }
    }
}
