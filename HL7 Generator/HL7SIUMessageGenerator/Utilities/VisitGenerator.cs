﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HL7SIUMessageGenerator.DataModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using HL7SIUMessageGenerator.Utilities;
using System.Diagnostics;

namespace HL7SIUMessageGenerator.Utilities
{
    public sealed class VisitGenerator
    {
        private static volatile VisitGenerator instance;
        
        private static object syncRoot = new Object();
        private List<String> _maleNames= new List<String>();
        private List<String> _femaleNames = new List<String>();
        private List<String> _surnames = new List<String>();
        private List<PatientNameIndex> _usedNames = new List<PatientNameIndex>();
        private Random _rng;

        public String UsedNameFilePath { get; set; }
        
        private VisitGenerator()
        {
            this._rng = new Random();
            _loadMaleNames();
            _loadFemaleNames();
            _loadSurnames();
        }

        ~VisitGenerator()
        {
            _saveUsedNames();
        }


        public static VisitGenerator Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new VisitGenerator();
                    }
                }

                return instance;
            }
        }

        //--Originally, this was not configurable :( booo...
        public void LoadUsedNames(String usedNameFilePath)
        {
            this.UsedNameFilePath = usedNameFilePath;
            _loadUsedNames();
        }

        public void TestDuplicateNameGeneration()
        {
            if (this._usedNames.Count > 0)
            {
                PatientNameIndex index = this._usedNames[0];
                PatientNameIndex copy = new PatientNameIndex();
                copy.NameIndex = index.NameIndex;
                copy.SurnameIndex = index.SurnameIndex;
                copy.Male = index.Male;

                if (!this._usedNames.Contains(copy))
                {
                    throw new Exception("Copy not found!");
                }
            }
        }

        private void _loadUsedNames()
        {
            if (File.Exists(this.UsedNameFilePath))
            {
                using(TextReader textReader = new StreamReader(this.UsedNameFilePath))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(List<PatientNameIndex>));
                    this._usedNames = (List<PatientNameIndex>)deserializer.Deserialize(textReader);
                }
            }
        }

        private void _loadMaleNames()
        {
            this._maleNames = FileTools.ListFromDelimitedFile(System.IO.Path.Combine(Environment.CurrentDirectory, Constants.MaleNameFilePath), Constants.NamesFileDelimiter);
        }

        private void _loadFemaleNames()
        {
            this._femaleNames = FileTools.ListFromDelimitedFile(System.IO.Path.Combine(Environment.CurrentDirectory, Constants.FemaleNameFilePath), Constants.NamesFileDelimiter);
        }

        private void _loadSurnames()
        {
            this._surnames = FileTools.ListFromDelimitedFile(System.IO.Path.Combine(Environment.CurrentDirectory, Constants.SurnameFilePath), Constants.NamesFileDelimiter);
        }

        private void _saveUsedNames()
        {
            using (Stream fileStream = File.Open(this.UsedNameFilePath, FileMode.OpenOrCreate))
            {
                TextWriter textWriter = new StreamWriter(fileStream);
                XmlSerializer serializer = new XmlSerializer(typeof(List<PatientNameIndex>));
                serializer.Serialize(textWriter, this._usedNames);
            }
        }

        private PatientNameIndex _generateName()
        {
            PatientNameIndex patientName = new PatientNameIndex();
            patientName.Male = Convert.ToBoolean(this._rng.Next(0,2));
            if( patientName.Male)
            {
                patientName.NameIndex = this._rng.Next(0, this._maleNames.Count);
            }
            else
            {
                patientName.NameIndex = this._rng.Next(0, this._femaleNames.Count);
            }

            patientName.SurnameIndex = this._rng.Next(0, this._surnames.Count);

            return patientName;
        }

        private String generateRandomDOBString()
        {
            return String.Format("{0}{1:D2}{2:D2}", this._rng.Next(1930, 2013),
                                              this._rng.Next(1, 13),
                                              this._rng.Next(1, 29));
        }

        private String generateRandomDOBStringUSOrder()
        {
            string dateString = generateRandomDOBString();
            return DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("M/d/yy");
        }

        public Patient GenerateRandomPatient()
        {
            PatientNameIndex patientNameIndex = new PatientNameIndex();
            bool isUnique = false;
            int iterations = 0;
            do
            {
                patientNameIndex = _generateName();
                if (!this._usedNames.Contains(patientNameIndex))
                    isUnique = true;

                iterations++;
            } while (!isUnique);

            Trace.WriteLine(String.Format("Took [{0}] iterations to create unused name", iterations));

            Patient patient = new Patient();
            patient.Gender = patientNameIndex.Male ? Constants.GenderMale : Constants.GenderFemale;
            patient.GenderJson = patientNameIndex.Male ? "Male" : "Female";

            if(patientNameIndex.Male)
            {
                patient.FirstName = this._maleNames[patientNameIndex.NameIndex];
            }
            else 
            {
                patient.FirstName = this._femaleNames[patientNameIndex.NameIndex];
            }

            patient.LastName = this._surnames[patientNameIndex.SurnameIndex];
            patient.DateOfBirth = generateRandomDOBString();
            patient.DateOfBirthUS = DateTime.ParseExact(patient.DateOfBirth, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("M/d/yy");
            patient.Mrn = IncrementalGenerator.Instance.NextMrn().ToString();
            patient.Visit = IncrementalGenerator.Instance.NextVisit().ToString();

            Trace.WriteLine(String.Format("Generated patient [{0}, {1}], [{2}], [{3}], [{4}], [{5}]", patient.LastName, patient.FirstName, patient.Gender, patient.DateOfBirth, patient.Mrn, patient.Visit));

            //--Add the name so it doesn't get used again.
            this._usedNames.Add(patientNameIndex);

            return patient;
        }

        public void SaveUsedNames()
        {
            _saveUsedNames();
        }
    }

}
