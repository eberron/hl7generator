﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HL7SIUMessageGenerator.Utilities
{
    public sealed class IncrementalGenerator
    {
        private static volatile IncrementalGenerator instance;
        private static object syncRoot = new Object();

        private int _lastMrn = 0;
        private int _lastVisit = 0;
        private int _lastPatientId = 0;

        private IncrementalGenerator() 
        {
            this._lastMrn = Properties.Settings.Default.LastInPatientMRNStartNumber;
            this._lastVisit = Properties.Settings.Default.LastInPatientVisitStartNumber;
            this._lastPatientId = Properties.Settings.Default.LastPatientId;
        }

        public static IncrementalGenerator Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new IncrementalGenerator();
                    }
                }

                return instance;
            }
        }

        public int NextMrn()
        {
            this._lastMrn++;
            Properties.Settings.Default.LastInPatientMRNStartNumber = this._lastMrn;
            Properties.Settings.Default.Save();
            return _lastMrn;
        }

        public int NextVisit()
        {
            this._lastVisit++;
            Properties.Settings.Default.LastInPatientVisitStartNumber = this._lastVisit;
            Properties.Settings.Default.Save();
            return _lastVisit;
        }

        public int NextPatientId()
        {
            this._lastPatientId++;
            Properties.Settings.Default.LastPatientId = this._lastPatientId;
            Properties.Settings.Default.Save();
            return _lastPatientId;
        }
    }


}
