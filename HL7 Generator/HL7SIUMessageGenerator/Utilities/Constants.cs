﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HL7SIUMessageGenerator.Utilities
{
    public static class Constants
    {
        public const String GenderMale = "M";
        public const String GenderFemale = "F";

        public const String NamesFileDelimiter = "|";
        public const String MaleNameFilePath = @"male_names.txt";
        public const String FemaleNameFilePath = @"female_names.txt";
        public const String SurnameFilePath = @"surnames.txt";
    }
}
