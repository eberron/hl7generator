﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using HL7SIUMessageGenerator.DataModel;

namespace HL7SIUMessageGenerator.Utilities
{
    public static class FileTools
    {        
        /// <summary>
        /// Read a complete file into a string
        /// </summary>
        /// <param name="path">File path</param>
        /// <returns>string</returns>
        public static string ReadFileString(string path)
        {
            // Use StreamReader to consume the entire text file.
            using (StreamReader reader = new StreamReader(path))
            {
                return reader.ReadToEnd();
            }
        }

        public static  List<String> ListFromDelimitedFile(String fileName, String delimiter)
        {
            List<String> separatedList = null;
            String delimitedString = FileTools.ReadFileString(fileName);
            if (delimitedString.Length > 0)
            {
                separatedList = new List<String>(delimitedString.Split(new String[] { delimiter }, StringSplitOptions.RemoveEmptyEntries));
            }
            return separatedList;
        }

        /// <summary>
        /// Find and Replace a token
        /// </summary>
        /// <param name="text">Text to search</param>
        /// <param name="token">Token to replace</param>
        /// <param name="replaceWith">Replace with string</param>
        /// <returns></returns>
        public static StringBuilder FindAndReplace(StringBuilder text, string token, string replaceWith)
        {
            return text.Replace(token, replaceWith);
        }
        /// <summary>
        /// This method parses the template and replaces with correct values and returns the final HL7 message as StringBuilder
        /// </summary>
        /// <param name="masterData">DataModel.MasterData</param>
        /// <returns>HL7 Message</returns>
        public static StringBuilder GetSingleOutPatientHL7Message(MasterData masterData)
        {
            StringBuilder text = generateCommonHL7MessageData(masterData);
            text = FindAndReplace(text, "<ApptReasonID>", masterData.Reason.ReasonId.Trim());
            text = FindAndReplace(text, "<ApptReasonText>", masterData.Reason.ReasonText.Trim());
            text = FindAndReplace(text, "<ApptDateTime>", masterData.AppointmentDate.ToString("yyyyMMddHHmmss"));
            return generateCareTeam(masterData, text);
        }

        public static StringBuilder generateCareTeam(MasterData masterData, StringBuilder text)
        {
            //--CareTeam
            //--Special case for admitting
            if (null != masterData.Dictator)
            {
                text = FindAndReplace(text, "<AdmittingPhysID>", masterData.Dictator.PhysicianCode.Trim());
                text = FindAndReplace(text, "<AdmittingPhysLastName>", masterData.Dictator.LastName.Trim());
                text = FindAndReplace(text, "<AdmittingPhysFirstName>", masterData.Dictator.FirstName.Trim());
            }
            else
            {
                text = FindAndReplace(text, "<AdmittingPhysID>", "");
                text = FindAndReplace(text, "<AdmittingPhysLastName>", "");
                text = FindAndReplace(text, "<AdmittingPhysFirstName>", "");
            }

            if (null != masterData.Attending)
            {
                text = FindAndReplace(text, "<AttendingPhysID>", masterData.Attending.IdNumber.Trim());
                text = FindAndReplace(text, "<AttendingPhysLastName>", masterData.Attending.LastName.Trim());
                text = FindAndReplace(text, "<AttendingPhysFirstName>", masterData.Attending.FirstName.Trim());
            }
            else
            {
                text = FindAndReplace(text, "<AttendingPhysID>", "");
                text = FindAndReplace(text, "<AttendingPhysLastName>", "");
                text = FindAndReplace(text, "<AttendingPhysFirstName>", "");
            }

            if (null != masterData.Consulting)
            {
                text = FindAndReplace(text, "<ConsultingPhysID>", masterData.Consulting.IdNumber.Trim());
                text = FindAndReplace(text, "<ConsultingPhysLastName>", masterData.Consulting.LastName.Trim());
                text = FindAndReplace(text, "<ConsultingPhysFirstName>", masterData.Consulting.FirstName.Trim());
            }
            else
            {
                text = FindAndReplace(text, "<ConsultingPhysID>", "");
                text = FindAndReplace(text, "<ConsultingPhysLastName>", "");
                text = FindAndReplace(text, "<ConsultingPhysFirstName>", "");
            }

            if (null != masterData.Referring)
            {
                text = FindAndReplace(text, "<ReferringPhysID>", masterData.Referring.IdNumber.Trim());
                text = FindAndReplace(text, "<ReferringPhysLastName>", masterData.Referring.LastName.Trim());
                text = FindAndReplace(text, "<ReferringPhysFirstName>", masterData.Referring.FirstName.Trim());
            }
            else
            {
                text = FindAndReplace(text, "<ReferringPhysID>", "");
                text = FindAndReplace(text, "<ReferringPhysLastName>", "");
                text = FindAndReplace(text, "<ReferringPhysFirstName>", "");
            }
            return text;
        }

        public static StringBuilder generateCommonHL7MessageData(MasterData masterData)
        {
            StringBuilder text = new StringBuilder(masterData.Template);
            text = FindAndReplace(text, "<MessageDateTime>", DateTime.Now.ToString("yyyyMMddHHmmss"));
            text = FindAndReplace(text, "<MRN>", masterData.Patient.Mrn.Trim());
            text = FindAndReplace(text, "<PatientLastName>", masterData.Patient.LastName.Trim());
            text = FindAndReplace(text, "<PatientFirstName>", masterData.Patient.FirstName.Trim());
            text = FindAndReplace(text, "<DOB>", masterData.Patient.DateOfBirth.Trim());
            text = FindAndReplace(text, "<Gender>", masterData.Patient.Gender.Trim());
            text = FindAndReplace(text, "<HomePhoneNumber>", ExpandPhoneNumber("888" + masterData.Patient.Mrn.Trim()));
            text = FindAndReplace(text, "<LocationID>", masterData.Location.Id.Trim());
            text = FindAndReplace(text, "<LocationText>", masterData.Location.Description.Trim());
            text = FindAndReplace(text, "<VisitID>", masterData.Patient.Visit);
            return text;
        }
        /// <summary>
        /// This method extends the outpatient function to include inpatient meta. See GetSingleOutPatientHL7Message().
        /// </summary>
        /// <param name="masterData">DataModel.MasterData</param>
        /// <returns>HL7 Message</returns>
        public static StringBuilder GetSingleInPatientHL7Message(MasterData masterData)
        {
            
            StringBuilder text = generateCommonHL7MessageData(masterData);
            
            text = FindAndReplace(text, "<MessageType>", "A01");
            text = FindAndReplace(text, "<AdmitDate>", masterData.AppointmentDate.ToString("yyyyMMddHHmmss"));
            text = FindAndReplace(text, "<DischargeDate>", "");

            //--Location
            if (null != masterData.Location.Floor)
                text = FindAndReplace(text, "<Floor>", masterData.Location.Floor.Trim());

            if (null != masterData.Location.Room)
                text = FindAndReplace(text, "<Room>", masterData.Location.Room.Trim());

            if (null != masterData.Location.Bed)
                text = FindAndReplace(text, "<Bed>", masterData.Location.Bed.Trim());

            
            //--Diagnosis
            text = FindAndReplace(text,"<DiagnosisCode>", masterData.Reason.ReasonId);
            text = FindAndReplace(text,"<DiagnosisDescription>", masterData.Reason.ReasonText);
            return generateCareTeam(masterData, text);
        }

        /// <summary>
        /// This method extends the inpatient function to include discharge meta. See GetSingleInPatientHL7Message().
        /// </summary>
        /// <param name="masterData">DataModel.MasterData</param>
        /// <returns>HL7 Message</returns>
        public static StringBuilder GetSingleInPatientDischargeHL7Message(MasterData masterData)
        {
            StringBuilder text = generateCommonHL7MessageData(masterData);

            text = FindAndReplace(text, "<MessageType>", "A03");
            text = FindAndReplace(text, "<AdmitDate>", masterData.AppointmentDate.ToString("yyyyMMddHHmmss"));
            text = FindAndReplace(text, "<DischargeDate>", masterData.DischargeDate.ToString("yyyyMMddHHmmss"));

            //--Location
            if (null != masterData.Location.Floor)
                text = FindAndReplace(text, "<Floor>", masterData.Location.Floor.Trim());

            if (null != masterData.Location.Room)
                text = FindAndReplace(text, "<Room>", masterData.Location.Room.Trim());

            if (null != masterData.Location.Bed)
                text = FindAndReplace(text, "<Bed>", masterData.Location.Bed.Trim());

            //--Diagnosis
            text = FindAndReplace(text, "<DiagnosisCode>", masterData.Reason.ReasonId);
            text = FindAndReplace(text, "<DiagnosisDescription>", masterData.Reason.ReasonText);

            return generateCareTeam(masterData, text);
        }

        /// <summary>
        /// Expand phone number to proper format
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string ExpandPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) { return string.Empty; }

            return Regex.Replace(phoneNumber, "(\\d{3})(\\d{3})(\\d{3})","($1)$2-$3");
        }
    }
}
