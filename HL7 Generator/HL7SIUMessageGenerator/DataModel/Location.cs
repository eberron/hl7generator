﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HL7SIUMessageGenerator.Utilities;


namespace HL7SIUMessageGenerator.DataModel
{
    public class Location
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Floor{get;set;}
        public string Room {get;set;}
        public string Bed {get;set;}

        /// <summary>
        /// Get Locations From File
        /// </summary>
        /// <param name="fileName">FileName</param>
        /// <returns>IList of CareTeamMember</returns>
        public static IList<Location> GetLocations(string fileName)
        {            
            DataTable data = CSVParser.ParseCSV(fileName);
            if (null == data || data.Rows.Count == 0)
            {
                return null;
            }
            IList<Location> items = new List<Location>();
            Location location = null;
            foreach (DataRow dr in data.Rows)
            {
                if (!string.IsNullOrEmpty(dr[0].ToString()))
                {
                    location = new Location()
                    {
                        Id = dr[0].ToString(),
                        Description = dr[1].ToString(),
                        Floor = dr[2].ToString(),
                        Room = dr[3].ToString(),
                        Bed = dr[4].ToString()
                    };
                    items.Add(location);
                }
            }
            return items;
        }
    }
}
