﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HL7SIUMessageGenerator.Utilities;

namespace HL7SIUMessageGenerator.DataModel
{
    public class Reason
    {
        public string ReasonId { get; set; }
        public string ReasonText { get; set; }

        /// <summary>
        /// Get Reasonf from file
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <returns>IList of Reason</returns>
        public static IList<Reason> GetReasons(string fileName)
        {
            IList<Reason> reasonList = new List<Reason>();
            Reason reason = null;
            DataTable reasonData = CSVParser.ParseCSV(fileName);
            if (null == reasonData || reasonData.Rows.Count == 0)
            {
                return null;
            }
            foreach (DataRow dr in reasonData.Rows)
            {
                if (!string.IsNullOrEmpty(dr[0].ToString()))
                {
                    reason = new Reason()
                    {
                        ReasonId = dr[0].ToString(),
                        ReasonText = dr[1].ToString()
                    };
                    reasonList.Add(reason);
                }
            }
            return reasonList;
        }
    }
}
