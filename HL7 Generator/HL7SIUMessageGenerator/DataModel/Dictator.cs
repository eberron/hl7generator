﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HL7SIUMessageGenerator.Utilities;

namespace HL7SIUMessageGenerator.DataModel
{
    /// <summary>
    /// Dictators
    /// </summary>
    public class Dictator
    {
        public string PhysicianCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public IList<Location> LocationList { get; set; }
        /// <summary>
        /// Get Dictators from CSV file
        /// </summary>
        /// <param name="fileName">FileName</param>
        /// <returns>IList Dictator</returns>
        public static IList<Dictator> GetDictators(string fileName)
        {
            IList<Dictator> dictatorList = new List<Dictator>();
            Dictator dictator = null;
            DataTable dictatorData = CSVParser.ParseCSV(fileName);
            if (null == dictatorData || dictatorData.Rows.Count == 0)
            {
                return null;
            }
            foreach (DataRow dr in dictatorData.Rows)
            {
                if (!string.IsNullOrEmpty(dr[0].ToString()))
                {
                    dictator = new Dictator()
                    {
                        PhysicianCode = dr[0].ToString(),
                        LastName = dr[1].ToString(),
                        FirstName = dr[2].ToString()
                    };
                    dictator.LocationList = new List<Location>();
                    for (int i = 0; i < (dictatorData.Columns.Count - 3); i = i + 2)
                    {
                        if (!string.IsNullOrEmpty(dr[i + 3].ToString()))
                        {
                            dictator.LocationList.Add(new Location
                            {
                                Id = dr[i + 3].ToString(),
                                Description = dr[i + 4].ToString()
                            });
                        }
                    }
                    dictatorList.Add(dictator);
                }
            }
            return dictatorList;
        }
    }
}
