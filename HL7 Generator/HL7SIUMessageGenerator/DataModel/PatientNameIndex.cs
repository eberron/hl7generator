﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HL7SIUMessageGenerator.DataModel
{
    [Serializable()]
    public struct PatientNameIndex
    {
        public int NameIndex{get;set;}
        public bool Male{get;set;}
        public int SurnameIndex{get;set;}
    }
}
