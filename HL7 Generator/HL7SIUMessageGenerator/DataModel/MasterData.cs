﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HL7SIUMessageGenerator.DataModel
{
    public class MasterData
    {
        public string Template {get;set;}
        public Dictator Dictator { get; set; } //--Will always been in the admitting role
        public Location Location { get; set; }
        public Patient Patient {get;set;}
        public CareTeamMember Attending { get; set; }
        public CareTeamMember Consulting { get; set; }
        public CareTeamMember Referring { get; set; }
        public Reason Reason { get; set; }
        public DateTime AppointmentDate { get; set; }
        public DateTime DischargeDate { get; set; }
    }
}
