﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using HL7SIUMessageGenerator.Utilities;
using System.Globalization;

namespace HL7SIUMessageGenerator.DataModel
{
    [DataContract]
    public class JsonPatient
    {
        public JsonPatient()
        { 
        }

        public JsonPatient(MasterData masterData, Dictator dictator)
        {
            Id = IncrementalGenerator.Instance.NextPatientId().ToString();

            Patient patient = masterData.Patient;
            
            Name = String.Format("{0}, {1}", patient.LastName, patient.FirstName);
            VisitNumber = patient.Visit;
            Mrn = patient.Mrn;
            DateOfBirth = patient.DateOfBirthUS;
            //TODO-Calculate
            TimeSpan ageSpan = ageSpan = DateTime.Now - DateTime.ParseExact(DateOfBirth, "M/d/yy", CultureInfo.InvariantCulture);
            double calculatedAge = 0.0;
            if (ageSpan.TotalDays > 364)
            {
                calculatedAge = ageSpan.TotalDays / 365.0;
                Age = ((int)calculatedAge).ToString() + "y";
            }
            else
            {
                calculatedAge = ageSpan.TotalDays / 30.0;
                Age = ((int)calculatedAge).ToString() + "m";
            }

            Gender = patient.GenderJson;
            Phone = FileTools.ExpandPhoneNumber("888" + patient.Mrn.Trim());
            
            AdmitDate = masterData.AppointmentDate.ToString("M/d/yy H:mm tt");
            DischargeDate = masterData.DischargeDate.ToString("M/d/yy");

            Reason = masterData.Reason.ReasonText;
            Admitting = string.Format("{0} {1}", dictator.FirstName, dictator.LastName);
            Attending = string.Format("{0} {1}", masterData.Attending.FirstName, masterData.Attending.LastName);
            Referring = string.Format("{0} {1}", masterData.Referring.FirstName, masterData.Referring.LastName);

            Floor = masterData.Location.Floor;
            Room = masterData.Location.Room;
            Bed = masterData.Location.Bed;

            DueForDischarge = "false";
            Treated = "false";
            Followup = "false";
        }

        [DataMember(Name="id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "visitnumber")]
        public string VisitNumber { get; set; }

        [DataMember(Name = "mrn")]
        public string Mrn { get; set; }

        [DataMember(Name = "dob")]
        public string DateOfBirth { get; set; }

        [DataMember(Name = "age")]
        public string Age { get; set; }

        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "admitdate")]
        public string AdmitDate { get; set; }

        [DataMember(Name = "dischargedate")]
        public string DischargeDate { get; set; }

        [DataMember(Name = "reason")]
        public string Reason { get; set; }

        [DataMember(Name = "admitting")]
        public string Admitting { get; set; }

        [DataMember(Name = "attending")]
        public string Attending { get; set; }

        [DataMember(Name = "referring")]
        public string Referring { get; set; }

        [DataMember(Name = "floor")]
        public string Floor { get; set; }

        [DataMember(Name = "room")]
        public string Room { get; set; }

        [DataMember(Name = "bed")]
        public string Bed { get; set; }

        [DataMember(Name = "duefordischarge")]
        public string DueForDischarge { get; set; }

        [DataMember(Name = "followup")]
        public string Followup { get; set; }

        [DataMember(Name = "treated")]
        public string Treated { get; set; }
    }

}
