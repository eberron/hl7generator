﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HL7SIUMessageGenerator.DataModel
{
    public class GeneratorData
    {
        public string Template {get;set;}
        public IList<Dictator> DictatorList {get;set;}
        public IList<Patient> PatientList {get;set;}
        public IList<CareTeamMember> CareTeamList {get;set;}
        public IList<Reason> ReasonList {get;set;}
        public IList<Location> LocationList { get; set; }

        public int NumberOfAppointments {get;set;} 
        public int AppointmentDuration {get;set;}
        public DateTime AppointmentDate {get;set;}
        public int AppointmentHour { get; set; }
        public int AppointmentMinute { get; set; }
        public string OutputFolder { get; set; }
        public string OutputDischargeFolder { get; set; }
        public bool InPatient { get; set; }
        public int DischargeFrequency { get; set; }
        public int DischargeInterval { get; set; }
        public float DischargeFactor { get; set; }
        public IList<CareTeamRole> careTeamRoles { get; set; }

        public int ImportDayOffset { get; set; }
        public bool UseGeneratedPatient { get; set; }
        public bool UseDischargeFolder { get; set; }
        public int DaysToGenerate { get; set; }
        public int ItemsPerDay { get; set; }
        public string DischargeList { get; set; }

    }
}
