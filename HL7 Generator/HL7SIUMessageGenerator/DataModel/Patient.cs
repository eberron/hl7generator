﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HL7SIUMessageGenerator.Utilities;
using System.Globalization;

namespace HL7SIUMessageGenerator.DataModel
{
    public class Patient
    {
        public string Mrn { get; set; }
        public string Visit { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfBirthUS { get; set; }
        public string Gender { get; set; }
        public string GenderJson { get; set; }

        /// <summary>
        /// Get Patient List from frile
        /// </summary>
        /// <param name="fileName">FileName</param>
        /// <returns>IList Patient</returns>
        public static IList<Patient> GetPatients(string fileName)
        {
            IList<Patient> patientList = new List<Patient>();
            Patient patient = null;
            DataTable patientData = CSVParser.ParseCSV(fileName);
            if (null == patientData || patientData.Rows.Count == 0)
            {
                return null;
            }
            foreach (DataRow dr in patientData.Rows)
            {
                if (!string.IsNullOrEmpty(dr[0].ToString()))
                {
                    patient = new Patient()
                    {
                        Mrn = dr[0].ToString(),
                        LastName = dr[1].ToString(),
                        FirstName = dr[2].ToString(),
                        DateOfBirth = dr[3].ToString(),
                        DateOfBirthUS = DateTime.ParseExact(dr[3].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("M/d/yy"),
                        Gender = dr[4].ToString(),
                        GenderJson = (0 == String.Compare(dr[4].ToString(), "m", true)) ? "Male" : "Female"
                    };
                    patientList.Add(patient);
                }
            }
            return patientList;
        }
    }
}
