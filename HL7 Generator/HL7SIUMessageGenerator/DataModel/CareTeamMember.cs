﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HL7SIUMessageGenerator.Utilities;


namespace HL7SIUMessageGenerator.DataModel
{
    public enum CareTeamRole
    { 
        Admitting,
        Attending,
        Consulting,
        Referring
    }

    public class CareTeamMember
    {
        public CareTeamRole role { get; set; }
        public string IdNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }

        public CareTeamMember()
        {
            LastName = "";
            FirstName = "";
            IdNumber = "";
        }

        /// <summary>
        /// Get CareTeamMember Physicians From File
        /// </summary>
        /// <param name="fileName">FileName</param>
        /// <returns>IList of CareTeamMember</returns>
        public static IList<CareTeamMember> GetCareTeamMembers(string fileName)
        {            
            DataTable data = CSVParser.ParseCSV(fileName);
            if (null == data || data.Rows.Count == 0)
            {
                return null;
            }
            IList<CareTeamMember> list = new List<CareTeamMember>();
            CareTeamMember member = null;
            foreach (DataRow dr in data.Rows)
            {
                if (!string.IsNullOrEmpty(dr[0].ToString()))
                {
                    member = new CareTeamMember()
                    {
                        IdNumber = dr[0].ToString(),
                        LastName = dr[1].ToString(),
                        FirstName = dr[2].ToString()
                    };
                    list.Add(member);
                }
            }
            return list;
        }
    }
}
